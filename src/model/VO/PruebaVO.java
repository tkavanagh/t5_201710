package model.VO;

public class PruebaVO {

	private String nombre;
	private int ano;
	
	
	public int compareTo(PruebaVO arg0) {
		if (arg0.getAno() > getAno()){
			return -1;
		}else if(arg0.getAno() < getAno()){
			return 1;
		}
		else if(arg0.getAno()== getAno()){
			if(arg0.getNombre() > getNombre()){
				return -1;
			}else if(arg0.getNombre() < getNombre()){
				return 1;
			}else
				return 0;
		}
	}


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public int getAno() {
		return ano;
	}


	public void setAno(int ano) {
		this.ano = ano;
	}

}
