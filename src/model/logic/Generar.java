package model.logic;

import java.util.Random;
import java.util.UUID;

public class Generar {
	
		private String[] pedro;
		private int[] juan;
		
		public Generar(int n){
			pedro = new String[n];
			juan = new int[n];
			
			for (int a=0;a<n;a++){
				Random rand = new Random();
				int randy = rand.nextInt(2017) + 1950;
				juan[n]= randy;
				pedro[n]=UUID.randomUUID().toString();	
			}
		}
		public String[] darStrings(){
			return pedro;
		}
		public int[] darInts(){
			return juan;
		}
		
}

